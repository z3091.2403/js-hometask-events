'use strict';

/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов
*/

/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
*/

menuBtn.onclick = function () {
  const menu = document.getElementById('menu');
  menu.style.display = menu.style.display === "block" ? "none" : "block";
}

const menuItem = document.getElementsByClassName('menu-item')
for (const item of menuItem) {
  item.onclick = function () {
    document.getElementById('menu').style.display='none';
  }
}
/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
*/
field.onclick = function moveBlock(event) {
  let cord = event.target.getBoundingClientRect()
  let block = document.querySelector('.moved-block')
  if(event.srcElement.id !== "movedBlock") {
    block.style.left = `${event.clientX - cord.left}px`
    block.style.top = `${event.clientY - cord.top}px`
  }
}
/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
*/
messager.onclick = function (event) {
  const elemEvent = event.target;
  if (elemEvent.className === "remove") elemEvent.parentElement.style.display = "none";
}
/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
*/
const linkArray = links.querySelectorAll('a')
for (const a of linkArray) {
  a.onclick = function (event)
 {
   if (!confirm("Вы точно хотите перейти по указанной ссылке?")) event.preventDefault()
 }  
}

/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
*/

fieldHeader.onchange = function () {
  taskHeader.innerText = fieldHeader.value;
}

